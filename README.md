# azrs-tracking

Repozitorijum za dokumentovanje primene alata obrađenih na kursu MATF-AZRS, nad projektom [CHIMP](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/04-chimp) (projekat na kursu MATF-RS). \
Profesor: [Ivan Čukić](https://cukic.co/) \
Asistent: [Momir Adžemović](http://www.matf.bg.ac.rs/p/-momir-adzemovic) \
\
Spisak alata, način na koji ih upotrebiti, kao i uputstva za upotrebu mogu se naći na sledećim adresama: \
[Spisak i detalji o zadacima u projektu](http://poincare.matf.bg.ac.rs/~ivan/?content=azrs) \
[Materijal sa vežbi](https://github.com/Robotmurlock/MATF-AZRS/blob/main/README.md)
